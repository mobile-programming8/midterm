import 'dart:io';
import 'dart:math';

void main() {
  //Ex : 1 + 1 * ( 9 - 3 ) + ( 12 / 2 ) * 5 Answer should be 37.0
  String? input = stdin.readLineSync();
  String string = input!;
  var list = string.split(' ');

  List<String> postfix = toPostFix(list);
  evaluatePostFix(postfix);
}

void evaluatePostFix(List<String> postfix) {
  List<double> values = [];
  for (var token in postfix) {
    if (isNumeric(token)) {
      values.add(double.parse(token));
    } else {
      double right = values.removeLast();
      double left = values.removeLast();
      switch (token) {
        case "+":
          values.add(left + right);
          break;
        case "-":
          values.add(left - right);
          break;
        case "*":
          values.add(left * right);
          break;
        case "/":
          values.add(left / right);
          break;
        default:
      }
    }
  }
  print(values);
}

List<String> toPostFix(List<String> list) {
  List<String> postfix = [];
  List<String> operators = [];
  for (var token in list) {
    if (isNumeric(token)) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (operators.isNotEmpty &&
          operators.last != "(" &&
          procedence(token) < procedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }
    if (token == "(") {
      operators.add(token);
    }
    if (token == ")") {
      while (operators.last != "(") {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }
  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

bool isNumeric(String str) {
  try {
    var value = double.parse(str);
  } on FormatException {
    return false;
  }
  return true;
}

bool isOperator(String s) {
  if (s == "+" || s == "-" || s == "*" || s == "/" || s == "^") {
    return true;
  }
  return false;
}

int procedence(String s) {
  switch (s) {
    case "+":
      return 1;
    case "-":
      return 1;
    case "*":
      return 2;
    case "/":
      return 2;
    case "^":
      return 3;
    case "(":
      return 4;
    default:
      return -1;
  }
}
